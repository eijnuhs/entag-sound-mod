package net.jolly.entagsound.mixin;

import net.jolly.entagsound.util.SoundEntityContainer;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.control.LookControl;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.jolly.entagsound.util.SoundHandler;

@Mixin(MobEntity.class)
public abstract class MobEntityMixin extends LivingEntity {

	// mixin placeholders
	@Shadow public abstract LookControl getLookControl();
	@Shadow public int ambientSoundChance;

	@Shadow public abstract int getMinAmbientSoundDelay();

	private int coolDown;
	protected MobEntityMixin(EntityType<? extends MobEntity> entityType, World world) {
		super(entityType, world);
		coolDown = SoundHandler.DEFAULT_COOLDOWN;
	}


	// todo Check for if entity is looking at player
	@Inject(at = @At("TAIL"), method = "tick()V")
	public void tick(CallbackInfo info) {
		SoundEntityContainer tagMob = SoundHandler.getKeyTaggedEntity(this);
		if(tagMob != null){
			/* this ensures that when mob is untag, it will return to normal. */
			if(this.ambientSoundChance >= 0){
				this.ambientSoundChance = -2*getMinAmbientSoundDelay();
			}
			if (--coolDown < 0) {
				// set to a small cool down to prevent unnecessary world player checks every tick
				coolDown = SoundHandler.DEFAULT_QUICK_COOLDOWN;
				PlayerEntity nearestPlayer = this.world.getClosestPlayer(this, 5);
				if (nearestPlayer != null){
					// todo: Should this be in ? Issue: play sound before the look action is complete.
					this.getLookControl().lookAt(nearestPlayer);
					// a player is close by, set new coolDown
					coolDown = tagMob.getCooldown();
					// play sound if pass random check pass
					if (tagMob.getChance() > this.getRandom().nextFloat()) {
						this.playSound(tagMob.getSoundEvent(), this.getSoundVolume(), this.getSoundPitch());
					}
				}
			}
		}
	}
}

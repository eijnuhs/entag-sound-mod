package net.jolly.entagsound;

import net.fabricmc.api.ModInitializer;
import net.jolly.entagsound.util.SoundHandler;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;

public class EntityTagSoundMain implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final String MOD_ID = "entagsound";
	public static final Logger LOGGER = LogManager.getLogger(MOD_ID);

	@Override
	public void onInitialize() {
		try {
			SoundHandler.loadConfigs();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

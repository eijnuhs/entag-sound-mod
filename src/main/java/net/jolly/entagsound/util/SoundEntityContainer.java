package net.jolly.entagsound.util;

import net.jolly.entagsound.EntityTagSoundMain;
import net.minecraft.entity.EntityType;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.Optional;

//todo add player specific field
public class SoundEntityContainer {
    private String name;
    private int cooldown;
    private float chance;
    private String soundid;
    private final ArrayList<String> entity;
    private transient SoundEvent soundEvent = null;
    private transient ArrayList<EntityType> keyEntityTypes = null;

    public SoundEntityContainer() {
        entity = new ArrayList<>();
    }

//    public final ArrayList<EntityType> getKeyEntityTypes() {
//        return keyEntityTypes;
//    }

    public boolean isKeyType(EntityType entityType){
        for( EntityType keyType : keyEntityTypes){
            if(entityType == keyType){
                return true;
            }
        }
        return false;
    }

    public final SoundEvent getSoundEvent() {
        return soundEvent;
    }

    public final int getCooldown() {
        return cooldown;
    }

    public final float getChance() {
        return chance;
    }

    public final String getName(){
        return name;
    }

    public String getSoundid() {
        return soundid;
    }

    public void initKeyEntityTypes(){
        if( keyEntityTypes == null){
            keyEntityTypes = new ArrayList<>();
        }
        for(String type : entity) {
            Optional<EntityType<?>> etype = EntityType.get(type);
            if(etype.isPresent()) {
                keyEntityTypes.add(etype.get());
            }
            else{
                EntityTagSoundMain.LOGGER.error("Error adding entity type: "+type);
            }
        }
    }

    public void registerSoundEvent(){
        Identifier soundID = new Identifier(EntityTagSoundMain.MOD_ID,soundid);
        SoundEvent soundEvent = new SoundEvent(soundID);
        Registry.register(Registry.SOUND_EVENT, soundID, soundEvent);
        this.soundEvent = soundEvent;
    }
}

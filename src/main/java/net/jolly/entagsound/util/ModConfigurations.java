package net.jolly.entagsound.util;

import java.util.ArrayList;

public class ModConfigurations {

    private String packname;
    private final ArrayList<SoundEntityContainer> tagnames;

    public ModConfigurations(){
        tagnames = new ArrayList<>();
    }

    public final ArrayList<SoundEntityContainer> getTagNames() {
        return tagnames;
    }
}
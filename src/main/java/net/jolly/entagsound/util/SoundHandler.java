package net.jolly.entagsound.util;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import net.fabricmc.loader.api.FabricLoader;
import net.jolly.entagsound.EntityTagSoundMain;

import net.minecraft.entity.LivingEntity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SoundHandler {

    // COOLDOWN time are in ticks
    public static final int DEFAULT_COOLDOWN = 600; // 30 sec to start
    public static final int DEFAULT_QUICK_COOLDOWN = 100; // 5 sec

    private static final String CONFIG_FILE_ERR = "Config JSON file MISSING/ERR.";
    private static final String CONFIG_DIR_ERR = "Config directory MISSING/ERR.";

    private static final HashMap<String,SoundEntityContainer> KEY_TAGS = new HashMap<>();
    public static ArrayList<ModConfigurations> configs = new ArrayList<>();

    /* Allow loading multiple resource pack config files */
    public static void loadConfigs() throws IOException {

        readJSONConfigs();
        for(ModConfigurations config : configs){
            registerSounds(config);
            initKeyEntities(config);
        }
    }

    public static SoundEntityContainer getKeyContainer(String key) {
        return KEY_TAGS.get(key);
    }

    public static SoundEntityContainer getKeyTaggedEntity(LivingEntity mob){
        // check if entity is custom tag
        if(!mob.hasCustomName()){
            return null;
        }
        // check if the custom tag name is in key list. Pass as lower case string.
        SoundEntityContainer tagMob = getKeyContainer(mob.getDisplayName().getString().toLowerCase());
        // check if the entity is in the key entity list
        return ( tagMob != null && tagMob.isKeyType(mob.getType())) ? tagMob : null;
    }

    private static void initKeyEntities(@NotNull ModConfigurations configs){
        for( SoundEntityContainer tag : configs.getTagNames() ){
            tag.initKeyEntityTypes();
        }
    }

    private static void registerSounds(@NotNull ModConfigurations configs){
        for( SoundEntityContainer tag : configs.getTagNames() ){
            // store as lower case so nametag can be case in-sensitive.
            String mobTag = tag.getName().toLowerCase();
            if(!KEY_TAGS.containsKey(mobTag)){
                KEY_TAGS.put(mobTag,tag);
                tag.registerSoundEvent();
            }
        }
    }

    private static void readJSONConfigs() throws IOException {
        String modConfigDir = FabricLoader.getInstance().getConfigDir()+ "/" + EntityTagSoundMain.MOD_ID;
        File configDir = new File(modConfigDir);
        if(!configDir.exists() && !configDir.mkdirs()){
            throw new IOException(CONFIG_DIR_ERR);
        }
        // Get all the JSON files from the directory
        File[] jsonFiles = configDir.listFiles(path -> path.getPath().endsWith(".json"));
        if(jsonFiles == null){
            throw new IOException(CONFIG_FILE_ERR);
        }
        for(File configFile : jsonFiles) {
            ModConfigurations conf = parseFromFile(configFile);
            if(conf != null){
                configs.add(conf);
            }
        }
    }

    private static @Nullable ModConfigurations parseFromFile(File file) {
        Gson gson = new Gson();
        try {
            JsonReader reader = new JsonReader(new FileReader(file));
            return gson.fromJson(reader, ModConfigurations.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


}

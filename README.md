# Entag Sound Mod 

A fabric mod for MC 1.17.1.  

Provide tagged entity with custom ambient sounds. This minecraft mod allows entity name tagged with a specific name to replace and play custom ambient sounds.
Any living entity (e.g. villager, zombie or custom entity) can be tagged and customised individually. So two villager
tagged with different names will play different set of sounds. 

## Setup

Prepare one or more JSON configuration files (check examples folder for an example) and put it under
minecraft's configuration folder (e.g. .minecraft/config/entagsound)

 * packname: Name of the resource pack pertaining to
 * tagnames: list of name tag configuration
   * name: the nametag's name
   * cooldown: time to play next sound
   * chance: Chance to play the sound 
   * entity: Set of entities that associates with the tag name. 
   * soundid: the sound ID to Minecraft to load/register

Then prepare and load a sound resource pack with the relevant sound IDs in the config file. 
Information on [how to create a resource pack](https://minecraft.fandom.com/wiki/Tutorials/Creating_a_resource_pack).

Multiple configuration files can be loaded.

## Usage

Simply nametag the chosen entity with its key name. To revert the sounds, simply change or remove the nametag name of the tagged entity.


